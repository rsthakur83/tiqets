FROM ubuntu:18.10
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install python3 python3-pip
 
RUN pip3 install flask
 
 
ENV FLASK_APP=web.app
ENV FLASK_DEBUG=1
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
 
WORKDIR /opt
COPY ./requirements.txt /opt/requirements.txt
COPY ./app.py /opt/app.py

RUN pip3 install -r requirements.txt
ENV FLASK_APP=app.py
ENV APP_AES_KEY=somekey234876283

CMD ["flask", "run", "--host=0.0.0.0"]
