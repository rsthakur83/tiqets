from flask import Flask, request
from Crypto.Cipher import AES
from Crypto import Random
import base64
import os
import logging

import logging.handlers
import flask_monitoringdashboard as dashboard
from flask_jwt import JWT, jwt_required

logging.basicConfig(filename='app.log', level=logging.INFO)

log = logging.getLogger()

encrypt_key = os.getenv('APP_AES_KEY')

#######

handler = logging.FileHandler('hello.log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s ---- %(threadName)s')
handler.setFormatter(formatter)
log.addHandler(handler)


#####


app = Flask(__name__)
#########3
app.config['SECRET_KEY'] = 'strongpassword'

USER_DATA = {
    "admin": "admin"
}

class User(object):
    def __init__(self, id):
        self.id = id

    def __str__(self):
        return "User(id='%s')" % self.id


def verify(username, password):
    if not (username and password):
        return False
    if USER_DATA.get(username) == password:
        return User(id=123)


def identity(payload):
    user_id = payload['identity']
    return {"user_id": user_id}

jwt = JWT(app, verify, identity)
####
dashboard.bind(app)
#####

BS = 16


def pad(s): return s + (BS - len(s) % BS) * chr(BS - len(s) % BS)


def unpad(s): return s[:-ord(s[len(s)-1:])]


def encrypt(raw):
    raw = pad(raw)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(encrypt_key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw))


def decrypt(enc):
    enc = base64.b64decode(enc)
    iv = enc[:16]
    cipher = AES.new(encrypt_key, AES.MODE_CBC, iv)
    return unpad(cipher.decrypt(enc[16:]))


@app.route('/encrypt', methods=['POST'])
@jwt_required()
def encrypt_route():
    #log.info("Encrypting {request.data.decode()}")
    return encrypt(request.data.decode())

@app.route('/decrypt', methods=['POST'])
@jwt_required()
def decrypt_route():
    log.info("Decrypted {decrypt(request.data)}")
    return decrypt(request.data)



############

